package com.example.model;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Document(collection = "Users")
@Entity
public class Users {

    @Id
    public String id;

    public String email;
    public String display_name;
//    list of puzzles that a user created
    public List<Puzzle> puzzles;

    public Users() {}

    public Users(String id, String email, String display_name, List<Puzzle> puzzles) {
        this.id = id;
        this.email = email;
        this.display_name = display_name;
        this.puzzles = puzzles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public List<Puzzle> getPuzzles() {
        return puzzles;
    }

    public void setPuzzles(List<Puzzle> puzzles) {
        this.puzzles = puzzles;
    }

    @Override
    public String toString() {
        return "Users {" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", display_name='" + display_name + '\'' +
                ", puzzles=" + puzzles +
                '}';
    }
}
