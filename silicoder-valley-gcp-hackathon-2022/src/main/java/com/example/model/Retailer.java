package com.example.model;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Document(collection = "Retailer")
@Entity
public class Retailer {

    @Id
    public String id;

//    list of products a retailer sells
    public List<Product> productList;

    public Retailer() {}

    public Retailer(String id, List<Product> productList) {
        this.id = id;
        this.productList = productList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "Retailer {" +
                "id='" + id + '\'' +
                ", productList=" + productList +
                '}';
    }
}
