package com.example.model;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import java.util.List;

@Document(collection = "Puzzles")
@Entity
public class Puzzle {
    @Id
    public String id;

    public String puzzleName;

    public String userEmail;

    // contains list of products that makes up a puzzle
    public List<Product> productList;

    // contains list of users that upvoted / favorited the puzzle
    public List<Users> upvoted_users;

    public int upvoted_users_size;

    public Puzzle() {}

    public Puzzle(String id, String puzzleName, String userEmail, List<Product> productList, List<Users> upvoted_users, int upvoted_users_size) {
        this.id = id;
        this.puzzleName = puzzleName;
        this.userEmail = userEmail;
        this.productList = productList;
        this.upvoted_users = upvoted_users;
        this.upvoted_users_size = upvoted_users_size;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPuzzleName() {
        return puzzleName;
    }

    public void setPuzzleName(String puzzleName) {
        this.puzzleName = puzzleName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public List<Users> getUpvoted_users() {
        return upvoted_users;
    }

    public void setUpvoted_users(List<Users> upvoted_users) {
        this.upvoted_users = upvoted_users;
    }

    public int getUpvoted_users_size() {
        return upvoted_users_size;
    }

    public void setUpvoted_users_size(int upvoted_users_size) {
        this.upvoted_users_size = upvoted_users_size;
    }

    @Override
    public String toString() {
        return "Puzzle {" +
                "id='" + id + '\'' +
                ", puzzleName='" + puzzleName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", productList=" + productList +
                ", upvoted_users=" + upvoted_users +
                ", upvoted_users_size=" + upvoted_users_size +
                '}';
    }
}
