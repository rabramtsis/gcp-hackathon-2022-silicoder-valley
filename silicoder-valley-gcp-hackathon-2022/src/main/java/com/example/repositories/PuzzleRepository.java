package com.example.repositories;

import com.example.model.Puzzle;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PuzzleRepository extends MongoRepository<Puzzle, String> {
}
