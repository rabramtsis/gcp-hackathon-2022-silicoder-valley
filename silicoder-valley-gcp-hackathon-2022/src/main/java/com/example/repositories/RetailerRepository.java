package com.example.repositories;

import com.example.model.Retailer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RetailerRepository extends MongoRepository<Retailer, String> {
}
