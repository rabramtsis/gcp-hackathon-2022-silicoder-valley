package com.example.services;

import com.example.model.Product;
import com.example.model.Puzzle;
import com.example.model.Users;
import com.example.repositories.PuzzleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PuzzleService {
    @Autowired
    PuzzleRepository repo;

    public List<Puzzle> getAll() {
        return repo.findAll();
    }

    public Optional<Puzzle> getPuzzle(String id) {
        return repo.findById(id);
    }
//    create puzzle
    public boolean createPuzzle(Puzzle p) {
        Optional<Puzzle> doesPuzzleExist = repo.findById(p.getId());
        if (doesPuzzleExist.isPresent()) {
            return false;
        }
        repo.save(p);
        return true;
    }

//    find puzzles by user's email
    public List<Puzzle> findPuzzlesByEmail(String email) {
        List<Puzzle> puzzles = new ArrayList<>();
        List<Puzzle> exist = repo.findAll();
        if (exist.isEmpty()) {
            return exist;
        }
        else {
            for (Puzzle p : exist) {
                if (p.getUserEmail() == email) {
                    puzzles.add(p);
                }
            }
        }
        return puzzles;
    }
//    find puzzles by product id
    public List<Puzzle> findPuzzlesByProduct(String pId) {
        List<Puzzle> puzzles = new ArrayList<>();
        List<Puzzle> exist = repo.findAll();
        if (exist.isEmpty()) {
            return exist;
        }
        else {
            for (Puzzle p : exist) {
//                for all products in a puzzle, if there is a match,
//                break out of current puzzle and move onto the next puzzle
                for (Product prod : p.getProductList()) {
                    if (prod.getId() == pId) {
                        puzzles.add(p);
                        break; // breaks out of inner for loop
                    }
                }
            }
        }
        return puzzles;
    }
//    find top x number of puzzles based on number of upvotes (popularity)
    public List<Puzzle> findTopTenPuzzles() {
        List<Puzzle> puzzles = new ArrayList<>();
        List<Puzzle> exist = repo.findAll();
        if (exist.isEmpty()) {
            return exist;
        }
        else {
//            sort puzzles into array of puzzles, highest upvote to lowest upvote
            Collections.sort(exist, Comparator.comparing(Puzzle::getUpvoted_users_size));
            Collections.reverse(exist);

//            grab top 10 puzzles (at end of list) and return as a list
            int counter = 0;
            for (Puzzle p : exist) {
                counter++;
                puzzles.add(p);
                if (counter >= 9) {
                    break;
                }
            }

        }
        return puzzles;
    }
//    edit puzzle
    public boolean updatePuzzle(String id, Puzzle updatedPuzzle) {
        Optional<Puzzle> doesPuzzleExist = repo.findById(id);
        if (doesPuzzleExist.isPresent()) {
            Puzzle up = doesPuzzleExist.get();

            up.setPuzzleName(updatedPuzzle.getPuzzleName());
            up.setProductList(updatedPuzzle.getProductList());
            up.setUpvoted_users(updatedPuzzle.getUpvoted_users());
            up.setUpvoted_users_size(updatedPuzzle.getUpvoted_users_size());

            repo.save(up);
            return true;
        }
        return false;
    }
//    delete puzzle
    public boolean deletePuzzle(String id) {
        try {
            repo.deleteById(id);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
}
