package com.example.services;

import com.example.model.Retailer;
import com.example.model.Users;
import com.example.repositories.RetailerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RetailerService {
    @Autowired
    RetailerRepository repo;

    public List<Retailer> getAll() {
        return repo.findAll();
    }

    public Optional<Retailer> getRetailer(String id) {
        return repo.findById(id);
    }

//    create a retailer
    public boolean createRetailer(Retailer r) {
        Optional<Retailer> doesRetailerExist = repo.findById(r.getId());
        if (doesRetailerExist.isPresent()) {
            return false;
        }
        repo.save(r);
        return true;
    }

//    edit product list
    public boolean updateRetailer(String id, Retailer updatedRetailer) {
        Optional<Retailer> doesRetailerExist = repo.findById(id);
        if (doesRetailerExist.isPresent()) {
            Retailer ur = doesRetailerExist.get();

            ur.setProductList(updatedRetailer.getProductList());

            repo.save(ur);
            return true;
        }
        return false;
    }

//    delete a retailer
    public boolean deleteRetailer(String id) {
        try {
            repo.deleteById(id);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
}
