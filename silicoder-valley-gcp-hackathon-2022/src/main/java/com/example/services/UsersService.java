package com.example.services;

import com.example.model.Product;
import com.example.model.Users;
import com.example.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsersService {
    @Autowired
    UsersRepository service;


    public List<Users> getAll() {
        return service.findAll();
    }

    public Optional<Users> getUsers(String id) {
        return service.findById(id);
    }

//    create new user
    public boolean createUser(Users u) {
        Optional<Users> doesUserExist = service.findById(u.getId());
        if (doesUserExist.isPresent()) {
            return false;
        }
        service.save(u);
        return true;
    }

//    edit user
    public boolean updateUser(String id, Users updatedUser) {
        Optional<Users> doesUserExist = service.findById(id);
        if (doesUserExist.isPresent()) {
            Users uu = doesUserExist.get();

            uu.setDisplay_name(updatedUser.getDisplay_name());
            uu.setPuzzles(updatedUser.getPuzzles());

            service.save(uu);
            return true;
        }
        return false;
    }

//    delete user
    public boolean deleteUser(String id) {
        try {
            service.deleteById(id);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
}
