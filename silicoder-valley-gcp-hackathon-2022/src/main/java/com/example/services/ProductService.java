package com.example.services;

import com.example.model.Product;
import com.example.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository repo;

//    creates and saves new product into repo
    public boolean createProduct(Product p) {
        Optional<Product> doesProductExist = repo.findById(p.getId());
        if (doesProductExist.isPresent()) {
            return false;
        }
        repo.save(p);
        return true;
    }

//    finds a product and returns to the controller
    public Optional<Product> getProduct(String id) {
        return repo.findById(id);
    }

//    updates quantity, price
    public boolean updateProduct(String id, Product updatedProduct) {
        Optional<Product> doesProductExist = repo.findById(id);
        if (doesProductExist.isPresent()) {
            Product up = doesProductExist.get();

            up.setPrice(updatedProduct.getPrice());
            up.setQuanitity(updatedProduct.getQuanitity());

            repo.save(up);
            return true;
        }
        return false;
    }

//    delete product
    public boolean deleteProduct(String id) {
        try {
            repo.deleteById(id);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

}
