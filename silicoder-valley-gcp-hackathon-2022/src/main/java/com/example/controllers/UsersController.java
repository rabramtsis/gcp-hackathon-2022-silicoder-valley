package com.example.controllers;

import com.example.model.Product;
import com.example.model.Users;
import com.example.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/user")
public class UsersController {

    @Autowired
    UsersService service;

    @GetMapping("/getAllUsers")
    public ResponseEntity<List<Users>> getAllCards () throws Exception {
        return new ResponseEntity<List<Users>>(service.getAll(), HttpStatus.OK);
    }

    @GetMapping("/getUser/{id}")
    @ResponseBody
    public ResponseEntity<?> getUsersBy(@PathVariable("id") String id){
        Optional<Users> users = service.getUsers(id);
        if (users.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(users.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @PostMapping("/createUser")
    @ResponseBody
    public ResponseEntity<?> createUser(@RequestBody Users users){
        boolean success = service.createUser(users);
        if (success){
            return ResponseEntity.status(HttpStatus.CREATED).body("User created");
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PutMapping("/updateUser/{id}")
    @ResponseBody
    public ResponseEntity<?> updateUser(@PathVariable("id") String id, @RequestBody Users users){
        boolean success = service.updateUser(id, users);
        if (success){
            return ResponseEntity.status(HttpStatus.OK).body("User Updated");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @DeleteMapping("/deleteUser/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteUser(@PathVariable("id") String id){
        boolean success = service.deleteUser(id);
        if (success){
            return ResponseEntity.status(HttpStatus.OK).body("User deleted");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
