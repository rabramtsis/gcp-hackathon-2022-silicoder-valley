package com.example.controllers;

import com.example.model.Puzzle;
import com.example.services.PuzzleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/puzzle")

public class PuzzleController {

    @Autowired
    PuzzleService service;

    @GetMapping("/getAllPuzzles")
    public ResponseEntity<List<Puzzle>> getAllPuzzles() throws Exception {
        return new ResponseEntity<List<Puzzle>>(service.getAll(), HttpStatus.OK);
    }

    @GetMapping("/getPuzzle/{id}")
    @ResponseBody
    public ResponseEntity<?> getPuzzlesById(@PathVariable("id") String id){
        Optional<Puzzle> puzzles = service.getPuzzle(id);
        if (puzzles.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(puzzles.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @PostMapping("/createPuzzle")
    @ResponseBody
    public ResponseEntity<?> createPuzzle(@RequestBody Puzzle puzzle){
        boolean success = service.createPuzzle(puzzle);
        if (success){
            return ResponseEntity.status(HttpStatus.CREATED).body("Puzzle created");
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @GetMapping("/getPuzzle")
    @ResponseBody
    public ResponseEntity<?> findPuzzlesByEmail(@RequestBody String user_email){
        List<Puzzle> puzzle = service.findPuzzlesByEmail(user_email);
        if (!puzzle.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(puzzle);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }


    @GetMapping("/getPuzzle/{products}")
    @ResponseBody
    public ResponseEntity<?> findPuzzlesByProduct(@PathVariable("products") String products_id){
        List<Puzzle> puzzle = service.findPuzzlesByProduct(products_id);
        if (!puzzle.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(puzzle);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @GetMapping("/getTopTenPuzzles")
    @ResponseBody
    public ResponseEntity<?> findTopTenPuzzles(){
        List<Puzzle> puzzle = service.findTopTenPuzzles();
        if (!puzzle.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(puzzle);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @PutMapping("/updatePuzzle/{id}")
    @ResponseBody
    public ResponseEntity<?> updatePuzzle(@PathVariable("id") String id, @RequestBody Puzzle puzzle){
        boolean success = service.updatePuzzle(id, puzzle);
        if (success){
            return ResponseEntity.status(HttpStatus.OK).body("Puzzle Updated");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @DeleteMapping("/deletePuzzle/{id}")
    @ResponseBody
    public ResponseEntity<?> deletePuzzle(@PathVariable("id") String id){
        boolean success = service.deletePuzzle(id);
        if (success){
            return ResponseEntity.status(HttpStatus.OK).body("Puzzle deleted");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
