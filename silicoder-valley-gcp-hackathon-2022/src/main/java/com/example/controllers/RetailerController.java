package com.example.controllers;

import com.example.model.Retailer;
import com.example.model.Users;
import com.example.services.RetailerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/retailer")

public class RetailerController {
    @Autowired
    RetailerService service;

    @GetMapping("/getAllRetailers")
    public ResponseEntity<List<Retailer>> getAllRetailers() throws Exception {
        return new ResponseEntity<List<Retailer>>(service.getAll(), HttpStatus.OK);
    }

    @GetMapping("/getRetailers/{id}")
    @ResponseBody
    public ResponseEntity<?> getRetailerById(@PathVariable("id") String id){
        Optional<Retailer> retailer = service.getRetailer(id);
        if (retailer.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(retailer.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @PostMapping("/createRetailer")
    @ResponseBody
    public ResponseEntity<?> createRetailer(@RequestBody Retailer retailer){
        boolean success = service.createRetailer(retailer);
        if (success){
            return ResponseEntity.status(HttpStatus.CREATED).body("Retailer created");
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PutMapping("/updateRetailer/{id}")
    @ResponseBody
    public ResponseEntity<?> updateRetailer(@PathVariable("id") String id, @RequestBody Retailer retailer){
        boolean success = service.updateRetailer(id, retailer);
        if (success){
            return ResponseEntity.status(HttpStatus.OK).body("Retailer Updated");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @DeleteMapping("/deleteRetailer/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteRetailer(@PathVariable("id") String id){
        boolean success = service.deleteRetailer(id);
        if (success){
            return ResponseEntity.status(HttpStatus.OK).body("Retailer deleted");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
