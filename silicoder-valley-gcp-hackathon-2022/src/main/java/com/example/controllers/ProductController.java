package com.example.controllers;

import com.example.model.Product;
import com.example.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/products")
public class ProductController {

    @Autowired
    ProductService service;

    @GetMapping("/getProduct/{id}")
    @ResponseBody
    public ResponseEntity<?> getProductById(@PathVariable("id") String id){
        Optional<Product> product = service.getProduct(id);
        if (product.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(product.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @PostMapping("/createProduct")
    @ResponseBody
    public ResponseEntity<?> createProduct(@RequestBody Product product){
        boolean success = service.createProduct(product);
        if (success){
            return ResponseEntity.status(HttpStatus.CREATED).body("Product created");
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PutMapping("/updateProduct/{id}")
    @ResponseBody
    public ResponseEntity<?> updateProduct(@PathVariable("id") String id, @RequestBody Product product){
        boolean success = service.updateProduct(id, product);
        if (success){
            return ResponseEntity.status(HttpStatus.OK).body("Product Updated");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @DeleteMapping("/deleteProduct/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteProduct(@PathVariable("id") String id){
        boolean success = service.deleteProduct(id);
        if (success){
            return ResponseEntity.status(HttpStatus.OK).body("Product deleted");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
